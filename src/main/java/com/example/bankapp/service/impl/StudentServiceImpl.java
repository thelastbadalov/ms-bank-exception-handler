package com.example.bankapp.service.impl;

import com.example.bankapp.entity.Student;
import com.example.bankapp.repository.StudentRepository;
import com.example.bankapp.service.StudentService;
import jakarta.persistence.criteria.Predicate;
import java.util.ArrayList;
import java.util.List;
import lombok.RequiredArgsConstructor;
import org.springframework.data.jpa.domain.Specification;
import org.springframework.stereotype.Service;

@Service
@RequiredArgsConstructor
public class StudentServiceImpl implements StudentService {

    private final StudentRepository studentRepository;

    @Override
    public Long saveStudent(Student student) {
        return studentRepository.save(student).getId();
    }

    @Override
    public List<Student> getAllStudents(String name, String surname) {

//        Specification<Student> studentSpecification = null;
//
//
//        studentSpecification = (root, cq, cb) -> {
//
//            List<Predicate> predicateList = new ArrayList<>();
//
//            if (name != null) {
//                predicateList.add(cb.equal(root.get("name"), name));
//            }
//
//            if (surname != null) {
//                predicateList.add(cb.equal(root.get("surname"), surname));
//            }
//
//            cq.where(cb.and(predicateList.toArray(new Predicate[0])));
//            return cq.getRestriction();
//
//        };
//
//        return studentRepository.findAll(studentSpecification);
        return null;
    }

    @Override
    public List<Student> getStudents(String name, String surname, Long age, String gender) {
        return studentRepository.getStudnets(name, surname, age, gender);
    }
}
