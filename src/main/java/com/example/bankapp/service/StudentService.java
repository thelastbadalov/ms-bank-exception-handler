package com.example.bankapp.service;

import com.example.bankapp.entity.Student;
import java.util.List;

public interface StudentService {
    Long saveStudent(Student student);

    List<Student> getAllStudents(String name, String surname);

    List<Student> getStudents(String name, String surname, Long age, String gender);
}
