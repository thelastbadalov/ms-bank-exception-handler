package com.example.bankapp.controller;

import com.example.bankapp.entity.Student;
import com.example.bankapp.repository.StudentRepository;
import lombok.Getter;
import lombok.RequiredArgsConstructor;
import org.springframework.data.crossstore.ChangeSetPersister;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RestController;

@RestController
@RequiredArgsConstructor
public class StudentController {

    private final StudentRepository studentRepository;

    @GetMapping("/student/{name}")
    public ResponseEntity<Student> getStudent(@PathVariable String name) {
        return ResponseEntity.ok(studentRepository.findByName(name).orElseThrow(RuntimeException::new));
    }
}
