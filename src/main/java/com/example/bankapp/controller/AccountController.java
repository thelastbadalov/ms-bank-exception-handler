package com.example.bankapp.controller;

import com.example.bankapp.dto.AccountDto;
import com.example.bankapp.entity.Account;
import com.example.bankapp.service.AccountService;
import com.example.bankapp.service.impl.AccountServiceImpl;
import java.util.List;
import lombok.RequiredArgsConstructor;
import org.springframework.web.bind.annotation.RestController;

@RestController
@RequiredArgsConstructor
public class AccountController {
    private AccountService accountService;

    public List<Account> getAccountDto() {
        return accountService.getAccountDto();
    }
}
