package com.example.bankapp;

import com.example.bankapp.entity.Student;
import com.example.bankapp.repository.AccountRepository;
import com.example.bankapp.repository.StudentRepository;
import com.example.bankapp.service.StudentService;
import java.util.List;
import lombok.RequiredArgsConstructor;
import org.springframework.boot.CommandLineRunner;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
@RequiredArgsConstructor
public class BankAppApplication implements CommandLineRunner {

    private final AccountRepository accountRepository;
    private final StudentService studentService;

    private final StudentRepository studentRepository;

    public static void main(String[] args) {
        SpringApplication.run(BankAppApplication.class, args);
    }

    @Override
    public void run(String... args) throws Exception {
//        accountRepository.findByCustom().forEach(System.out::println);
//        accountRepository.findByAccountNumber("iba123").forEach(System.out::println);
//        accountRepository.findByCustomGraph().forEach(System.out::println);
//        accountRepository.findByAccountNumber("iba123").forEach(System.out::println);
//        accountRepository.findByAccountNumber("iba123").forEach(System.out::println);
        Student qulu = Student.builder()
                .name("Qulu")
                .surname("Bedelov")
                .age(20)
                .gender("M")
                .build();
        Student mehemmed = Student.builder()
                .name("Mehemmed")
                .surname("Ilyazov")
                .age(20)
                .gender("M")
                .build();
        Student esref = Student.builder()
                .name("Esref")
                .surname("Sukurlu")
                .age(20)
                .gender("M")
                .build();
        List<Student> students = List.of(qulu, mehemmed, esref);
        for (Student student : students) {
//            studentService.saveStudent(student);
        }

//        List<Student> allStudents = studentService.getAllStudents("Qulu", null);
//        allStudents.forEach(System.out::println);

//        List<Student> studentList = studentService.getStudents("Qulu", "Bedelov", 21L, null);
//        studentList.forEach(System.out::println);

    }
}
